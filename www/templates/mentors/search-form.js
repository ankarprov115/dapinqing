this_page = function() {
    return {
        get_data: function() {
            data = {};
            data.post = $.core.qs();

            return data;
        },

        after_load: function() {
            // Autocomplete
            $('.autocomplete').each(function(i, elem) {
                $(elem).core_autocomplete($(this).data('autocomplete-src')+'/autocomplete', function(suggestion) {
                    // Update checkboxes/radios
                    $(elem).closest('.dropdown-container').find('input[type="radio"], input[type="checkbox"]').prop('checked', false);
                    $(elem).closest('.dropdown-container').find('input[name="'+$(elem).attr('name').replace(/^q_(.*)$/, '$1')+'-'+suggestion.data+'"]').prop('checked', true);
                });
            });

            // fix footer z-index
            $('.dropdown').on('show.bs.dropdown', function () {
                $(this).find('ul').addClass('show-element');
            });
            $('.dropdown').on('hide.bs.dropdown', function () {
                $(this).find('ul').removeClass('show-element');
            });
        },

        before_submit: function(data) {
            // Show loading
            $.core.show_loading();

            // Go to search results
            uri = '#mentors?'+$.param(data);
            $.core.redirect(uri);
            return false;
        }
    };
};