this_page = function() {
    return {
        get_data: function() {
            error_msg = $.i18n._('Unable to load data.');

            // Get industries from API
            data = $.core.get_from_api('multi/?requests={"industries": {}, "regions": {}, "company_types": {}, "company_sizes": {}}', error_msg);

            return data;
        },

        after_load: function() {
            // Show back button
            $.core.show_back_arrow();
            $('.cover-photo-cropper').hide();
            // Init file upload
            $('#register-employers-2 input[name="cover_photo"]').file_upload('image', 'profile_pic', 'user-profile_pic', function(fileuri) {
                // Crop
                $.core.cropper.init('#register-employers-2', fileuri, {
                    ready: function() {
                        $('#register-employers-2 input[type="submit"]').parent().hide();
                    },

                    applied: function(data) {
                        // Update pic in DOM
                        $('#register-employers-2 input#profile_pic').val(data.image_uri+'?$.now()');
                        $('nav .userPic').css('background-image', "url('"+data.image_uri+'?$.now()'+"')");

                        $('#register-employers-2 input[type="submit"]').parent().show();
                    },

                    cancelled: function() {
                        $('#register-employers-2 input[type="submit"]').parent().show();
                    }
                });
            });
            $('#register-employers-2 input[name="resume_en"]').file_upload('image', 'resume_en_uri', 'user-resume_en_uri');
            $('#register-employers-2 input[name="resume_zh"]').file_upload('image', 'resume_zh_uri', 'user-resume_zh_uri');
            $('#register-employers-2 input[name="video"]').file_upload('video', 'raw_video_uri', 'user-video');

            // Dropdowns
            $('.dropdown-toggle').dropdown();
            $(document).on('click', '.yamm .dropdown-menu', function(e) {
                e.stopPropagation();
            });

            // Update 'single-tier' radio selections
            $.each(['company_type', 'company_size'], function(i, thing) {
                $('.dropdown-menu.'+thing+' input[type="radio"]').off('change');
                $('.dropdown-menu.'+thing+' input[type="radio"]').on('change', function(e) {
                    selections = {'': {}};
                    $('.dropdown-menu.'+thing+' input[type="radio"]').each(function(i, elem) {
                        if ($(elem).is(':checked')) {
                            selections[''][$(this).closest('label').text().trim()] = 1;
                        }
                    });

                    selections_str = $.pages['global']._parse_dropdown_selections(selections);

                    $('input[name="'+thing.replace(/s$/, '')+'"]').val(selections_str);

                    $(this).closest('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle'); return false;

                    e.preventDefault();
                    return false;
                });
            });

            // Disabled no-input inputs
            $('.dropdown.no-input .dropdown-toggle input[type="text"]').click(function(e) {
                $(this).blur();
            });

            // dropdown toggle with footer
            $('.company-type').on('show.bs.dropdown', function () {
                $(this).find('ul').addClass('position-relative');
            });
            $('.company-type').on('hide.bs.dropdown', function () {
                $(this).find('ul').removeClass('position-relative');
            });

            $('.company-size').on('show.bs.dropdown', function () {
                $(this).find('ul').addClass('position-relative');
            });
            $('.company-size').on('hide.bs.dropdown', function () {
                $(this).find('ul').removeClass('position-relative');
            });

            // 'x' buttons handlers
            $('.dropdown a.x').off('click');
            $('.dropdown a.x').click(function(e) {
                $(this).closest('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle'); return false;

                e.preventDefault();
                return false;
            });
        },

        before_submit: function(data) {
            delete data.company_type;
            delete data.company_size;

            var $cover_photo = $('.cropper > img');
            data.cover_photo = $cover_photo.cropper('getData');

            return data;
        },

        register_cb: function(response) {
            data = $.core.parse_api_response(response);
            if (!data)
                return $.core.api_error(response, $.i18n._('Unable to save your account.')+' '+$.i18n._('Please try again.'));

            if (typeof(data.user) == 'object') {
                // Update session user
                $.core.session.set('user', data.user);

                // Update display
                $.core.update_dom();

                // Show success
                $.core.show_success($.i18n._('Thank you for completing your profile.'));

                // Redirect to home
                window.setTimeout("$.mobile.navigate('#home')", 1500);
            }else{
                // Show error
                $.core.show_success($.i18n._('Unable to refresh your display.')+' '+$.i18n._('Please log back in.'));

                // Redirect to login
                window.setTimeout("$.mobile.navigate('#login')", 1500);
            }
        }
    };
};