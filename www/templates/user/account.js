this_page = function() {
    return {
        get_data: function() {
            error_msg = $.i18n._('Unable to load your account data.');

            // Get user data
            data = $.core.get_from_api('multi/?requests={"users/me?with=blocked_companies": {"as": "me"}, "industries": {"as-schema": 1}, "regions": {"by_first_letter": 1, "as": "locations"}, "company_types": {}, "company_sizes": {}, "education_requirements": {}, "languages": {}, "wage-ranges": {"as": "wage_ranges"}, "availabilities": {}, "experience_requirements": {}, "positions": {"as-schema": 1}}', error_msg);

            if (!$.isEmptyObject(data)) {
                data.regions = $.core.get_from_api('regions', error_msg);

                $.my_data = data.me;

                return data;
            }else
                $.mobile.navigate('#home');
        },

        after_load: function() {
            // Init autocomplete
            $('.autocomplete').each(function(i, elem) {
                $(elem).core_autocomplete($(this).data('autocomplete-src')+'/autocomplete', function(suggestion) {
                    if ($(elem).attr('name') == 'blocked_company') {
                        // Update selected-bubbles
                        $(elem).closest('[data-role="fieldcontainer"]').find('.selected-bubbles').append('<li><a href="#" onclick="$(this).parent().remove(); return false;"><span>'+suggestion.value+'</span><input type="hidden" name="blocked_company_ids[]" value="'+suggestion.data+'"></a></li>');
                        $(elem).val('');
                    }
                });
            });

            // Show back button
            $.core.show_back_arrow();

            // Only keep the form pertaining to the current user type
            if (!$.core.session.get('user').is_mentor) {
                $('#account-mentor-info-form').remove();
            }
            if (!$.core.session.get('user').is_student) {
                $('#account-student-info-form').remove();
            }

            // Free/paid selector
            $('#account-mentor-info-form input[type="radio"][name="price-choice"]').change(function() {
                if ($(this).val() == 'paid')
                    $('#account-mentor-info-form .enter-price').slideDown();
                else
                    $('#account-mentor-info-form .enter-price').slideUp();
            });

            // Highlight current tab
            if ($('body').pagecontainer('getActivePage').attr('id') == 'my-account-mentor-info') {
                $('#account-personal-info-form').hide();
                $('#account-mentor-info-form').show();
                $('#my-account-mentor-info-tab').addClass('current');
            }else if ($('body').pagecontainer('getActivePage').attr('id') == 'my-account-student-info') {
                $('#account-personal-info-form').hide();
                $('#account-student-info-form').show();
                $('#my-account-student-info-tab').addClass('current');
            }else
                $('#my-account-personal-info-tab').addClass('current');

            // Init file uploads
            $('#my-account input[name="cover_photo"]').file_upload('image', 'profile_pic', 'user-profile_pic', '#edit-user-profile_pic', function(fileuri) {
                $('#my-account .progress-container').hide();

                // Crop
                $.core.cropper.init('#my-account', fileuri, {
                    ready: function() {
                        $('#my-account input[type="submit"]').parent().hide();
                    },

                    applied: function(data) {
                        // Update pic in DOM
                        $('#my-account .user-header img.profile_pic').attr('src', data.image_uri+'?$.now()');
                        $('#my-account input#profile_pic').val(data.image_uri+'?$.now()');
                        $('#my-account .user-header').css('background-image', 'url("'+data.image_uri+'?$.now()'+'")');

                        $('#my-account input[type="submit"]').parent().show();

                        // Show success
                        $.core.show_success($.i18n._('Your image has been cropped.')+'<br>'+$.i18n._('Please click save below to save the change to your profile.'));
                    },

                    cancelled: function() {
                        $('#my-account input[type="submit"]').parent().show();
                    }
                });
            });
            if ($.core.session.get('user').is_mentor) {
                $('#account-mentor-info-form input[name="resume_en"]').file_upload('image', 'resume_en_uri', 'user-resume_en_uri');
                $('#account-mentor-info-form input[name="resume_zh"]').file_upload('image', 'resume_zh_uri', 'user-resume_zh_uri');
                $('#account-mentor-info-form input[name="video"]').file_upload('video', 'raw_video_uri', 'user-video');

                // Video player
                $.video_player = {
                    video_id: null,
                    player: null,
                    player_id: null,
                    is_ready: false,
                    is_playing: false,
                    is_seeking: false
                };
                $.fn.init_video = function() {
                    if (typeof($.my_data) == 'undefined')
                        return;

                    // Stash id
                    $.video_player.video_id = $.my_data.id;

                    // Load video into DOM
                    if (!$.my_data.video_uri)
                        return;
                    $(this).append('<video id="video-video" class="video" preload="auto" style="width: 100%;" autobuffer controls="false" poster="'+$.my_data.profile_pic+'"><source src="'+$.my_data.video_uri+'" type="video/mp4"></video>');

                    // Grab DOM stuff
                    $.video_player.player = $(this).find('video');
                    $.video_player.player_id = $.video_player.player.attr('id');

                    // Start stats listener
                    $.video_player.player.bind('canplay', function() {});
                    $.video_player.player.bind('play', function() {
                        $.video_player.is_playing = true;
                    });
                    $.video_player.player.bind('canplay', function() {});
                    $.video_player.player.bind('timeupdate', function() {});
                    $.video_player.player.bind('ended', function() { $.video_player.is_playing = false; });
                    $.video_player.player.bind('pause', function() { $.video_player.is_playing = false; });

                    // Video Loader
                    $.video_player.player.bind('loadstart', function() {
                        $(this).addClass('loading');
                        $(this).attr("poster", "assets/images/video-loading.gif");
                    });
                    $.video_player.player.bind('canplay', function() {
                        $(this).removeClass('loading');
                        $(this).removeAttr("poster");
                    });
                };
                $.fn.remove_video = function() {
                    $(this).html('<img src="app-core/assets/images/invalid-video.png" alt="Invalid Video" style="display: block; max-width: 100%; max-height: 200px; margin: 0 auto;">');
                };
                $('.account-video-container').init_video();
            }

            // Toggle
            $('.toggle-content').hide();
            $('.toggle-btn.active').removeClass('active');
            $('.toggle-btn').off('click');
            $('.toggle-btn').on('click', function(e){
                // Close all other toggles
                if (!$(this).hasClass('active'))
                    $('.toggle-btn.active').removeClass('active').next('.toggle-content').slideUp('fast');

                // Toggle
                $(this).toggleClass('active').next('.toggle-content').slideToggle('fast');
                e.preventDefault();
            });

            // Dropdowns
            $('.dropdown-toggle').dropdown();
            $(document).on('click', '.yamm .dropdown-menu', function(e) {
                e.stopPropagation()
            });

            // Update 'single-tier' radio selections
            $.each(['company_type', 'company_size', 'education'], function(i, thing) {
                $('.dropdown-menu.'+thing+' input[type="radio"]').off('change');
                $('.dropdown-menu.'+thing+' input[type="radio"]').on('change', function(e) {
                    selections = {'': {}};
                    $('.dropdown-menu.'+thing+' input[type="radio"]').each(function(i, elem) {
                        if ($(elem).is(':checked')) {
                            selections[''][$(this).closest('label').text().trim()] = 1;
                        }
                    });

                    selections_str = $.pages['global']._parse_dropdown_selections(selections);

                    $('input[name="'+thing.replace(/s$/, '')+'"]').val(selections_str);

                    $(this).closest('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle'); return false;

                    e.preventDefault();
                    return false;
                });
            });

            // Disabled no-input inputs
            $('.dropdown.no-input .dropdown-toggle input[type="text"]').click(function(e) {
                $(this).blur();
            });

            // 'x' buttons handlers
            $('.dropdown a.x').off('click');
            $('.dropdown a.x').click(function(e) {
                $(this).closest('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle'); return false;

                e.preventDefault();
                return false;
            });

            // Update current industry input upon radio selection
            $('.dropdown-menu.current_industry input[type="radio"]').off('change');
            $('.dropdown-menu.current_industry input[type="radio"]').on('change', function(e) {
                selections = {};
                $('.dropdown-menu.current_industry input[type="radio"]').each(function(i, elem) {
                    if ($(elem).is(':checked')) {
                        group = $(elem).closest('.item').siblings('.group').text().trim();

                        if (typeof(selections[group]) == 'undefined')
                            selections[group] = {};
                        selections[group][$(this).closest('label').text().trim()] = 1;
                    }
                });

                selections_str = $.pages['global']._parse_dropdown_selections(selections);

                $('input[name="current_industry"]').val(selections_str);

                // Close dropdown
                $(this).closest('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle'); return false;
            });

            // Update current position input upon radio selection
            $('.dropdown-menu.current_position input[type="radio"]').off('change');
            $('.dropdown-menu.current_position input[type="radio"]').on('change', function(e) {
                selections = {};
                $('.dropdown-menu.current_position input[type="radio"]').each(function(i, elem) {
                    if ($(elem).is(':checked')) {
                        group = $(elem).closest('.toggle-content').siblings('.toggle-btn').text().trim();

                        if (typeof(selections[group]) == 'undefined')
                            selections[group] = {};
                        selections[group][$(this).closest('label').text().trim()] = 1;
                    }
                });

                selections_str = $.pages['global']._parse_dropdown_selections(selections);

                $('input[name="current_position"]').val(selections_str);

                // Close dropdown
                $(this).closest('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle'); return false;
            });

            // Update desired positions input upon checkbox selection
            $('.dropdown-menu.desired_positions input[type="checkbox"]').off('change');
            $('.dropdown-menu.desired_positions input[type="checkbox"]').on('change', function(e) {
                selections = {};
                $('.dropdown-menu.desired_positions input[type="checkbox"]').each(function(i, elem) {
                    if ($(elem).is(':checked')) {
                        group = $(elem).closest('.toggle-content').siblings('.toggle-btn').text().trim();

                        if (typeof(selections[group]) == 'undefined')
                            selections[group] = {};
                        selections[group][$(this).closest('label').text().trim()] = 1;
                    }
                });

                selections_str = $.pages['global']._parse_dropdown_selections(selections);

                $('input[name="desired_positions"]').val(selections_str);
            });

            // Update desired locations input upon checkbox selection
            $('.dropdown-menu.desired_regions input[type="checkbox"]').off('change');
            $('.dropdown-menu.desired_regions input[type="checkbox"]').on('change', function(e) {
                selections = {};
                $('.dropdown-menu.desired_regions input[type="checkbox"]').each(function(i, elem) {
                    if ($(elem).is(':checked')) {
                        group = $(elem).closest('.toggle-content').siblings('.toggle-btn').text().trim();

                        if (typeof(selections[group]) == 'undefined')
                            selections[group] = {};
                        selections[group][$(this).closest('label').text().trim()] = 1;
                    }
                });

                selections_str = $.pages['global']._parse_dropdown_selections(selections);

                $('input[name="desired_regions"]').val(selections_str);
            });

            // Update desired industries input upon checkbox selection
            $('.dropdown-menu.desired_industries input[type="checkbox"]').off('change');
            $('.dropdown-menu.desired_industries input[type="checkbox"]').on('change', function(e) {
                selections = {};
                $('.dropdown-menu.desired_industries input[type="checkbox"]').each(function(i, elem) {
                    if ($(elem).is(':checked')) {
                        group = $(elem).closest('.item').siblings('.group').text().trim();

                        if (typeof(selections[group]) == 'undefined')
                            selections[group] = {};
                        selections[group][$(this).closest('label').text().trim()] = 1;
                    }
                });

                selections_str = $.pages['global']._parse_dropdown_selections(selections);

                $('input[name="desired_industries"]').val(selections_str);
            });

            // Update 'single-tier' radio selections
            $.each(['desired_wage_range_id', 'languages_spoken'], function(i, thing) {
                $('.dropdown-menu.'+thing+' input[type="radio"]').off('change');
                $('.dropdown-menu.'+thing+' input[type="radio"]').on('change', function(e) {
                    selections = {'': {}};
                    $('.dropdown-menu.'+thing+' input[type="radio"]').each(function(i, elem) {
                        if ($(elem).is(':checked')) {
                            selections[''][$(this).closest('label').text().trim()] = 1;
                        }
                    });

                    selections_str = $.pages['global']._parse_dropdown_selections(selections);

                    $('input[name="'+thing.replace(/s$/, '')+'"]').val(selections_str);

                    // Close dropdown
                    $(this).closest('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle'); return false;
                });
            });

            $('.dropdown-menu.languages_spoken input[type="checkbox"]').off('change');
            $('.dropdown-menu.languages_spoken input[type="checkbox"]').on('change', function(e) {
                selections = {};
                $('.dropdown-menu.languages_spoken input[type="checkbox"]').each(function(i, elem) {
                    if ($(elem).is(':checked')) {
                        group = $(elem).closest('.toggle-content').siblings('.toggle-btn').text().trim();

                        if (typeof(selections[group]) == 'undefined')
                            selections[group] = {};
                        selections[group][$(this).closest('label').text().trim()] = 1;
                    }
                });

                selections_str = $.pages['global']._parse_dropdown_selections(selections);

                $('input[name="languages_spoken"]').val(selections_str);
            });

            // fix footer z-index
            $('.dropdown').on('show.bs.dropdown', function () {
                $(this).find('ul').addClass('position-relative');
            });
            $('.dropdown').on('hide.bs.dropdown', function () {
                $(this).find('ul').removeClass('position-relative');
            });

        },

        before_submit: function(data) {
            // Remove 'illegal' fields from POST data
            delete data.company_type;
            delete data.company_size;
            delete data.education;
            delete data.blocked_company;
            delete data.current_industry;
            delete data.current_position;
            delete data.desired_industries;
            delete data.desired_positions;
            delete data.desired_regions;
            delete data.languages_spoken;
            $.each(data, function(k, v) {
                if (k.match(/(desired_industries|desired_positions|languages_spoken)-[0-9]*$/))
                    delete data[k];
            });

            // Parse dropdown selections
            $.each(['desired_industries', 'desired_positions', 'desired_regions', 'languages_spoken'], function(i, thing) {
                regexp = new RegExp('^'+thing+'-([0-9]*)$');
                data[thing+'_ids'] = [];
                $.each($('input[type="checkbox"]'), function(i, elem) {
                    k = $(elem).attr('name');
                    if (k.match(regexp)) {
                        id = k.replace(regexp, '$1');

                        if ($(elem).is(':checked')) {
                            data[thing+'_ids'].push(id);
                            data[thing+'-'+id] = 1;
                        }

                        delete data[thing+'-'+id];
                    }
                });
            });

            return data;
        },

        update_cb: function(response) {
            data = $.core.parse_api_response(response);
            if (!data)
                return $.core.api_error(response, $.i18n._('Unable to save your account information.')+' '+$.i18n._('Please try again.'));

            // Get updated user data from API
            $.core.get_from_api('users/me', null, function(response) {
                error = false;
                data = $.core.parse_api_response(response);
                if (!data) {
                    error = true;
                }else{
                    if (typeof(data.data) == 'undefined') {
                        error = true;
                    }else{
                        user = data.data;

                        // Update session user data
                        user = $.extend($.core.session.get('user'), user);
                        $.core.session.set('user', user);

                        // Update lang
                        $.i18n.set_lang(user.lang);
                    }
                }

                // Update lang
                if (error) {
                    $.i18n.set_lang($('select[name="lang"]').val());

                    return $.core.show_error('Unable to refresh your account information.');
                }
            });

            $.core.show_success($.i18n._('Your account information has been saved.'));

            // Reload if we are on mentor-info screen
            if ($('#account-mentor-info-form').is(':visible'))
                window.setTimeout(function() {
                    $.core.redirect('#my-account-mentor-info');
                }, 1500);
        }
    };
};

