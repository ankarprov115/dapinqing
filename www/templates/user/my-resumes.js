this_page = function() {
    return {
        get_data: function() {
            error_msg = $.i18n._('Unable to load your résumé data.');

            // Get user data
            data = $.core.get_from_api('multi/?requests={"users/me": {"as": "me"}}', error_msg);

            if (!$.isEmptyObject(data)) {
                // Require student
                if (!data.me.is_student) {
                    $.core.show_error($.i18n._('You must have an employee account to view that screen.'));

                    window.setTimeout("$.mobile.navigate('#my-account')", 1500);

                    return false;
                }

                $.my_data = data.me;

                return data;
            }else
                $.mobile.navigate('#home');
        },

        after_load: function() {
            // Show back button
            $.core.show_back_arrow();

            // Init file uploads
            $('#my-resumes input[name="resume_en"]').file_upload('image', 'resume_en_uri', 'user-resume_en_uri');
            $('#my-resumes input[name="resume_zh"]').file_upload('image', 'resume_zh_uri', 'user-resume_zh_uri');
            $('#my-resumes input[name="video"]').file_upload('video', 'raw_video_uri', 'user-video');

            // Video player
            $.video_player = {
                video_id: null,
                player: null,
                player_id: null,
                is_ready: false,
                is_playing: false,
                is_seeking: false
            };
            $.fn.init_video = function() {
                if (typeof($.my_data) == 'undefined')
                    return;

                // Stash id
                $.video_player.video_id = $.my_data.id;

                // Load video into DOM
                if (!$.my_data.video_uri)
                    return;
                $(this).append('<video id="video-video" class="video" preload="auto" style="width: 100%;" autobuffer controls="false" poster="'+$.my_data.profile_pic+'"><source src="'+$.my_data.video_uri+'" type="video/mp4"></video>');

                // Grab DOM stuff
                $.video_player.player = $(this).find('video');
                $.video_player.player_id = $.video_player.player.attr('id');

                // Start stats listener
                $.video_player.player.bind('canplay', function() {});
                $.video_player.player.bind('play', function() {
                    $.video_player.is_playing = true;
                });
                $.video_player.player.bind('canplay', function() {});
                $.video_player.player.bind('timeupdate', function() {});
                $.video_player.player.bind('ended', function() { $.video_player.is_playing = false; });
                $.video_player.player.bind('pause', function() { $.video_player.is_playing = false; });

                // Video Loader
                $.video_player.player.bind('loadstart', function() {
                    $(this).addClass('loading');
                    $(this).attr("poster", "assets/images/video-loading.gif");
                });
                $.video_player.player.bind('canplay', function() {
                    $(this).removeClass('loading');
                    $(this).removeAttr("poster");
                });
            };
            $.fn.remove_video = function() {
                $(this).html('<img src="app-core/assets/images/invalid-video.png" alt="Invalid Video" style="display: block; max-width: 100%; max-height: 200px; margin: 0 auto;">');
            };
            $('.resume-video-container').init_video();

            // fix footer z-index
            $('.dropdown').on('show.bs.dropdown', function () {
                $(this).find('ul').addClass('position-relative');
            });
            $('.dropdown').on('hide.bs.dropdown', function () {
                $(this).find('ul').removeClass('position-relative');
            });

        },

        before_submit: function(data) {
            return data;
        },

        update_cb: function(response) {
            data = $.core.parse_api_response(response);
            if (!data)
                return $.core.api_error(response, $.i18n._('Unable to save your résumés.')+' '+$.i18n._('Please try again.'));

            // Get updated user data from API
            $.core.get_from_api('users/me', null, function(response) {
                error = false;
                data = $.core.parse_api_response(response);
                if (!data) {
                    error = true;
                }else{
                    if (typeof(data.data) == 'undefined') {
                        error = true;
                    }else{
                        user = data.data;

                        // Update session user data
                        user = $.extend($.core.session.get('user'), user);
                        $.core.session.set('user', user);

                        // Update lang
                        $.i18n.set_lang(user.lang);
                    }
                }

                // Update lang
                if (error) {
                    $.i18n.set_lang($('select[name="lang"]').val());

                    return $.core.show_error('Unable to refresh your résumés.');
                }
            });

            $.core.show_success($.i18n._('Your résumés have been saved.'));
        }
    };
};

