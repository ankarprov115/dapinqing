this_page = function() {
    return {
        get_data: function() {
            error_msg = $.i18n._('Unable to load your account data.');

            // Get user data
            data = $.core.get_from_api('multi/?requests={"users/me": {"as": "me"}, "videos/mine": {"as": "mine"}, "videos/viewed": {"as": "viewed"}}', error_msg);

            if (!$.isEmptyObject(data)) {
                // Require mentor
                if (!data.me.is_mentor) {
                    $.core.show_error($.i18n._('You must have an employer account to view that screen.'));

                    window.setTimeout("$.mobile.navigate('#home')", 1500);

                    return false;
                }

                return data;
            }else
                $.mobile.navigate('#my-account');
        },

        after_load: function() {
            // Show back button
            $.core.show_back_arrow();

            // Flipsnaps
            $.core.flipsnap('#my-videos-mine');
            $.core.flipsnap('#my-videos-viewed');
        }
    };
};