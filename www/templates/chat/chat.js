this_page = function() {
    return {
        poll_interval: 5000,
        active: true,

        get_data: function() {
            if (!$.core.qs('t'))
                $.mobile.navigate('#interview?id='+$.core.qs('id')+'&t='+$.now());

            data = {};
            error_msg = $.i18n._('Unable to load interview session.');

            // Get user data
            id = $.core.qs('id');
            if (id)
                data = $.core.get_from_api('1-on-1-sessions/'+id, error_msg);
            else
                $.core.show_error(error_msg);

            if (!$.isEmptyObject(data)) {
                // Mark messages as read
                $.core.put_to_api('1-on-1-sessions/'+id+'?mark-messages-as-read', {}, null);

                // Globalize data
                $.chat_data = data;

                return data;
            }else{
                $.mobile.navigate('#interview-requests');
            }
        },

        after_load: function() {
            // Show back button
            $.core.show_back_arrow();

            // Resize chat window
            $.pages['chat.chat'].resize_chat_window();
            $('#chat').css({
                'overflow-x': 'hidden',
                'overflow-y': 'scroll',
                'resize': 'vertical'
            });

            // Grab id
            id = $('#vars').data('id');

            if (id) {
                // Poll for new messages
                window.setTimeout("$.pages['chat.chat'].update_messages("+id+");", $.pages['chat.chat'].poll_interval);
            }

            // Force send button to work
            $('#interview #chat-form input[type="submit"]').on('touchstart', function(e) {
                $('#interview #chat-form').submit();
                e.stopPropagation();
                return false;
            });

            // Scroll to bottom
            $('#chat').scrollTop($('#chat').height());

            // Connect to video server
            $.pages['chat.chat'].video_connect($.chat_data.video_session_id, $.chat_data.video_token);
        },

        resize_chat_window: function() {
            h = $(window).outerHeight() - $('.header_wrapper').outerHeight() - parseInt($('main').css('padding-top')) - parseInt($('main').css('padding-bottom')) - $('.footer_wrapper').outerHeight() - $('.messageReply').outerHeight();
            $('#chat').height(h+'px');
        },

        update_messages: function(id) {
            if (!$.pages['chat.chat'].active)
                return;

            if (id) {
                // Get unread messages
                data = $.core.get_from_api('1-on-1-messages/?session_id='+id+'&type=theirs&unread=true', null)

                if (!$.isEmptyObject(data)) {
                    // Mark messages as read
                    $.core.put_to_api('1-on-1-sessions/'+id+'?mark-messages-as-read', {}, null)

                    for (i=0; i < data.length; i++) {
                        // Add new message(s) DOM
                        $.pages['chat.chat'].add_message('theirs', data[i].message, data[i].created_at);
                    }
                }
            }

            // Do it again!
            window.setTimeout("$.pages['chat.chat'].update_messages("+id+");", $.pages['chat.chat'].poll_interval);
        },

        video_connect: function(session_id, token) {
            if (OT.checkSystemRequirements() == 1) {
                $.chat_data.session = OT.initSession($.chat_data.video_key, $.chat_data.video_session_id);

                me = {id: $.chat_data.mentor_id, name: $.chat_data.mentor_name, profile_pic: $.chat_data.mentor_profile_pic};
                them = {id: $.chat_data.user_id, name: $.chat_data.user_name, profile_pic: $.chat_data.user_profile_pic};

                if ($.chat_data.my_id != $.chat_data.mentor_id) {
                    them2 = me;
                    me = them;
                    them = them2;
                    delete them2;
                }

                $.chat_data.connection_count = 0;
                $.chat_data.session
                    .on('streamCreated', function(event) {
                        subscriber = $.chat_data.session.subscribe(event.stream, 'their-chat-video', {
                            width: '100%',
                            height: '100%',
                            name: them.name
                        },
                        function(error) {
                            if (error) {
                                $.core.show_error($.i18n._('Unable to join video stream.')+' '+$.i18n._('Please try again.'));
                                if ($.core.debug_mode)
                                    console.log(error.message);
                                return;
                            }

                            if (subscriber.stream.hasVideo) {
                                subscriber.setStyle('backgroundImageURI', subscriber.getImgData());
                            }else{
                                subscriber.setStyle('backgroundImageURI', them.profile_pic);
                            }
                        })
                        .on('videoElementCreated', function(event) {
                            container = $(subscriber.element).parent();
                            container.css('height', (container.width() * .67)+'px');
                        });
                    })
                    .on('connectionCreated', function (event) {
                        $.chat_data.connection_count++;

                        if ($.core.debug_mode)
                            console.log($.chat_data.connection_count + ' connections.');

                        if ($.chat_data.connection_count == 2)
                            $('#their-chat-video').html('<div class="joining">'+them.name+' is joining...</div>');
                    })
                    .on('connectionDestroyed', function (event) {
                        $.chat_data.connection_count--;

                        if ($.core.debug_mode)
                            console.log($.chat_data.connection_count + ' connections.');

                        $('#their-chat-video').html('').css('height', '0px');
                    })
                    .on('sessionDisconnected', function sessionDisconnectHandler(event) {
                        // The event is defined by the SessionDisconnectEvent class
                        $.core.show_notice($.i18n._('You have been disconnected from the video session.'));

                        if ($.core.debug_mode)
                            console.log('Disconnected from the TokBox session.');

                        $('#disconnectBtn').hide();

                        if (event.reason == 'networkDisconnected') {
                            $.core.show_error($.i18n._('Your video session has ended because your network connection terminated.'));
                        }
                    })
                    .connect($.chat_data.video_token, function(error) {
                        $.chat_data.publisher = OT.initPublisher('my-chat-video', {
                            width: '100%',
                            height: '100%',
                            name: me.name
                        })
                        .on('videoElementCreated', function(event) {
                            container = $($.chat_data.publisher.element).parent();
                            container.css('height', (container.width() * .67)+'px');
                        });

                        $.chat_data.session.publish($.chat_data.publisher);
                    });

                $.chat_data.session.connect(token, function(error) {
                    if (error) {
                        $.core.show_error($.i18n._('Unable to join video session.')+' '+$.i18n._('Please try again.'));

                        if ($.core.debug_mode)
                            console.log("TokBox error connecting: ", error.code, error.message);
                    }else{
                        if ($.core.debug_mode)
                            console.log("Connected to TokBox session.");
                    }
                });
            }else{
                $.core.show_error($.i18n._('Your browser down not support WebRTC for video chat.')+' '+$.i18n._('Video chat will be disabled.'));
            }
        },

        video_disconnect: function() {
            if (typeof($.chat_data) != 'undefined') {
                $.chat_data.session.disconnect();
            }
        },

        before_submit: function() {
            if (!$.trim($('#interview textarea[name="message"]').val()))
                return false;
        },

        after_submit: function(response) {
            success = false;
            try {
                response = $.parseJSON(response.responseText);

                if (typeof(response.inserts) != 'undefined')
                    if (response.inserts.length) {
                        success = true;

                        response = response.inserts[0];
                    }
            }catch (e) {}

            if (success) {
                // Clear input
                $('#interview #message').val('');
                $('#interview #message').blur();
                $('#interview #message').height('20px');

                // Add message to DOM
                $.pages['chat.chat'].add_message('mine', response.message, response.created_at);
            }else
                $.core.show_error($.i18n._('Oops!')+' '+$.i18n._('Unable to send your message.'));
        },

        add_message: function(type, content, time) {
            // Add message to DOM
            message = $('#chat .message').eq(0).clone();

            $(message).removeClass('theirs').removeClass('mine').addClass(type).data('time', time);
            $(message).text(content);
            $(message).show();

            $('#chat .message').last().after(message);

            // Resize chat window
            $.pages['chat.chat'].resize_chat_window();

            // Scroll to bottom
            $('#chat').scrollTop($('#chat').height());
        },

        before_leave: function() {
            if (!$.pages['chat.chat'].active)
                return;

            // Disconnect from video
            $.pages['chat.chat'].video_disconnect();

            // Stop text message polling
            $.pages['chat.chat'].active = false;
        }
    };
};